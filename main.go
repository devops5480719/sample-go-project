package main

import (
	"fmt"
	"net/http"
	"path/filepath"
)

func helloHandler(w http.ResponseWriter, r *http.Request) {
	helloFile := filepath.Join("templates", "hello.html")
	http.ServeFile(w, r, helloFile)
}

func main() {
	fs := http.FileServer(http.Dir("templates"))
	http.Handle("/", fs)

	// Add the helloHandler for a specific route
	http.HandleFunc("/hello", helloHandler)

	fmt.Println("Server starting on port 8080...")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		fmt.Println("Failed to start server:", err)
	}
}
